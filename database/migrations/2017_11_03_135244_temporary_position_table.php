<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemporaryPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pozicija')->nullable();
            $table->string('naziv', 255)->nullable();
            $table->string('sastojci', 255)->nullable();
            $table->string('slika', 255)->nullable();
            $table->string('kategorija', 255)->nullable();
            $table->string('cena', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
