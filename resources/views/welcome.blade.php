<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kafana_4.0</title>
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Satisfy|Bree+Serif|Candal|PT+Sans">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>
<body>
<!--banner-->
<section id="banner">
    <div class="bg-color">
        <header id="header">
            <div class="container">
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <a href="#about">About</a>
                    <a href="#menu-list">Menu</a>
                    <a href="admin/login">Admin</a>
                    <a href="auth/login">Login</a>
                </div>
                <!-- Use any element to open the sidenav -->
                <span onclick="openNav()" class="pull-right menu-icon">☰</span>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="inner text-center">
                    <h1 class="logo-name">Lorem</h1>
                    <h2>Lorem ipsum dolor sit </h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / banner -->
<!--about-->
<section id="about" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center marb-35">
                <h1 class="header-h">Lorem Ipsum</h1>
                <p class="header-p">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                    <br>nibh euismod tincidunt ut laoreet dolore magna aliquam. </p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="col-md-6 col-sm-6">
                    <div class="about-info">
                        <h2 class="heading">vel illum qui dolorem eum</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero impedit inventore culpa vero accusamus in nostrum dignissimos modi, molestiae. Autem iusto esse necessitatibus ex corporis earum quaerat voluptates quibusdam dicta!</p>
                        <div class="details-list">
                            <ul>
                                <li><i class="fa fa-check"></i>Lorem ipsum dolor sit amet, consectetur adipiscing </li>
                                <li><i class="fa fa-check"></i>Quisque finibus eu lorem quis elementum</li>
                                <li><i class="fa fa-check"></i>Vivamus accumsan porttitor justo sed </li>
                                <li><i class="fa fa-check"></i>Curabitur at massa id tortor fermentum luctus</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <img src="https://tasteserbia.com/wp-content/uploads/2014/12/kafana.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</section>
<!--/about-->
<!-- menu -->
<section id="menu-list" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center marb-35">
                <h1 class="header-h">Menu List</h1>
                <p class="header-p">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                    <br>nibh euismod tincidunt ut laoreet dolore magna aliquam. </p>
            </div>
            <div class="col-md-12  text-center gallery-trigger">
                <ul>
                @foreach($category as $key=>$cat)
                        <li><a class="filter" data-filter=".category-{{ $cat->kategorija }}"  >{{ $cat->kategorija }}</a></li>
                    @endforeach
                </ul>
                </div>
            </div>
            <div id="Container">

                @foreach($dorucak as $key=>$dor)
                <div class="mix category-{{ $dor->kategorija }} menu-restaurant" data-myorder="2">
                        <span class="clearfix"><img class='slika' src="{{ asset('images/'. $dor->slika) }}" alt="" height="55px" width="55px">
                        <a class="menu-title"  data-meal-img="assets/img/restaurant/rib.jpg">{{ $dor->naziv }}</a>
                        <span class="menu-price">{{ $dor->cena }}</span>
                        </span>
                        <span class="menu-subtitle">{{ $dor->sastojci }}</span>
                </div>
                    @endforeach
    </div>
    <a href="#banner" id="to-top" >Nazad na pocetak</a>
    </div>
</section>

<!--/ menu -->

<!-- / contact -->
<!-- footer -->
<footer class="footer text-center">
<div class="container">
    <div class="row">
        <div class="col-md-3 kontakti" >
            <p>Kontakt: 061/140-3576</p><br>
            <p>Adresa:Lukijana Musickog 46</p><br>
            <p>Facebook:jsdjfhahfd</p><br>
        </div>
        <div class="col-md-6 ">
            <form action="welcome" method="post">
                {{ csrf_field() }}
                <textarea name="komentari" id="komentari" cols="50" rows="10" placeholder="Polje za komentare" maxlength="500"></textarea>
                <button type="submit" class="btn btn-info form-control" >Posalji</button>
            </form>
        </div>
    </div>
</div>
</footer>
<!-- / footer -->

<script src="js/jquery.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/custom.js"></script>
<script src="contactform/contactform.js"></script>

</body>
</html>