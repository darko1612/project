@extends('layouts.admindashboard')
@section('title')
    Meni
@endsection
    @include('layouts.welcomehead')
@section('menu')
    @include('admin.adminplus')
    <body>
    <div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <button type="button" class="btn btn-info" id="add">Dodaj Novi Artikal</button>
            <a href="/admin/kategorije" type="button" class="btn btn-info" >Upravljanje Kategorijama</a>
            <a href="/admin/menu" type="button" class="btn btn-success" style="float: right">Sve Kategorije Zajedno</a>
        @foreach($category as $key=>$cat)
                <form action="/{{ $cat->kategorija }}/admin/menu" method="post" class="">
                    {{ csrf_field() }}
                    <input value="{{ $cat->kategorija }}" type="submit" class="btn btn-default" style="float: right; margin: 15px 5px;">
                </form>
            @endforeach
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>Naziv</th>
                <th>Sastojci</th>
                <th>Slika</th>
                <th>Kategorija</th>
                <th>Cena</th>
                </thead>
                <tbody>
                @yield('adminedit')
                @foreach($podaci as $key => $pod)
                    <tr>
                        <td>{{ $pod->naziv }}</td>
                        <td  style="overflow:hidden; max-width:120px ;">{{ $pod->sastojci }}</td>
                        <td><img src="{{ asset('images/'. $pod->slika) }}" alt="" height="55px" width="55px"></td>
                        <td>{{ $pod->kategorija }}</td>
                        <td>{{ $pod->cena }}</td>
                        <td>

                            <div class="row " style="float:right">
                                <div class="col-lg-3">

                                    <form action="/admin/{{ $pod->id }}/edit" method="get" id="update">
                                        {{ csrf_field()}}
                                        <button  class="btn btn-success form-group" id="update">
                                            <span class="glyphicon glyphicon-edit"></span>Izmeni</button>
                                    </form>
                                </div>
                                <div class="col-lg-3">
                                    <form action="/admin/{{$pod->id}}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger  form-group">
                                            <span class="glyphicon glyphicon-trash"></span>Obrisi</button>
                                    </form>
                                </div>
                                <div class="col-lg-3">
                                    <form action="/admin/{{ $pod->id }}/menu" method="get">
                                        {{ csrf_field() }}
                                        <button class="btn btn-info  form-group">
                                            <span class="glyphicon glyphicon-arrow-up"></span> Gore</button>
                                    </form>
                                </div>
                                <div class="col-lg-3">
                                    <form action="/admin/menu/{{ $pod->id }}" method="post">
                                        {{ csrf_field() }}
                                        <button class="btn btn-info  form-group">
                                            <span class="glyphicon glyphicon-arrow-down"></span> Dole</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
{{$podaci->links()}}
        </div>
        <script>

    $('#add').on('click', function() {
        $('#meni').modal('show');
    });
    (function($){
        window.onbeforeunload = function(e){
            window.name += ' [' + $(window).scrollTop().toString() + '[' + $(window).scrollLeft().toString();
        };
        $.maintainscroll = function() {
            if(window.name.indexOf('[') > 0)
            {
                var parts = window.name.split('[');
                window.name = $.trim(parts[0]);
                window.scrollTo(parseInt(parts[parts.length - 1]), parseInt(parts[parts.length - 2]));
            }
        };
        $.maintainscroll();
    })(jQuery);
        </script>
    </div>

</div>

</body>
@endsection