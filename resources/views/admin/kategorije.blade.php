
@extends('layouts.admindashboard')
@section('categories')
<h2>Kategorije</h2>
<form action="/admin/kategorije/" method="post" >
    {{ csrf_field() }}
    <input type="text" placeholder="Dodaj kategoriju" id="categories" name="categories">
    <button type="submit" class="btn btn-primary btn-large">Dodaj Kategoriju</button>
</form>

<table>
    <thead>
    <th>Kategorija</th>
    </thead>
@foreach($category as $key=>$cat)
        <tbody>

        <tr>
            <td>{{ $cat->kategorija }}</td>
            <td><div class="row " style="float:right">
                    <div class="col-lg-3">

                        <form action="/admin/{{$cat->id}}/kategorije" method="post">
                            {{ csrf_field()}}
                            <button  class="btn btn-success form-group" id="change">
                                <span class="glyphicon glyphicon-edit"></span>Izmeni</button>
                        </form>
                    </div>
                    <div class="col-lg-3">
                <form action="/admin/kategorije/{{ $cat->id }}" method="post">
                    {{ csrf_field() }}
                    {{method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger btn-large">Obrisi</button>
                </form>
                    </div>
                    <div class="col-lg-3">
                        <form action="{{ $cat->id }}/menu" method="get">
                            {{ csrf_field() }}
                            <button class="btn btn-info  form-group">
                                <span class="glyphicon glyphicon-arrow-up"></span> Gore</button>
                        </form>
                    </div>
                    <div class="col-lg-3">
                        <form action="menu/{{ $cat->id }}" method="post">
                            {{ csrf_field() }}
                            <button class="btn btn-info  form-group">
                                <span class="glyphicon glyphicon-arrow-down"></span> Dole</button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
</table>
<script>

    $('#change').on('click', function() {
        $('#category').modal('show');
    });
    </script>

@endsection