
@section('title')
    Kreiranje Menija
@endsection
@include('layouts.welcomehead')
<body>
<div  id="edit" role="dialog">
<div class="modal-dialog">
    <div class="modal-content">

        <div class="modal-body">
            <form action="/admin/{{ $podaci->id }}" method="post" enctype="multipart/form-data">
                {{method_field('PUT')}}
                <div class="row">
                    {{ csrf_field() }}
                    <div class="col-lg-4 col-sm-4">
                        <div class="form-group">
                            <input type="text" name="naziv" id="naziv" placeholder="Naziv Artikla" value="{{ $podaci->naziv}}" class="form-control">
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4">
                        <div class="form-group">
                            <input type="file"  name="slika" id="slika" placeholder="Slika" value="{{ $podaci->slika}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="form-group">
                            <select  name="kategorija" id="kategorija" class="form-control">
                                <option value="{{$podaci->kategorija}}">{{$podaci->kategorija}}</option>
                                @foreach($category as $key=>$cat)
                                    <option value="{{$cat->kategorija}}">{{$cat->kategorija}}</option>
                                    @endforeach
                                    </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="form-group">
                            <input type="text" name="cena" id="cena" placeholder="Cena" value="{{ $podaci->cena }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="form-group">
                                <textarea style="overflow:auto;" name="sastojci" id="sastojci" rows="5" cols= "50" maxlength="255"
                                          placeholder="Sastojci" class="form-control">{{ $podaci->sastojci }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="save" class="btn btn-info">Sacuvaj</button>
                    <button type="submit" class="btn btn-default" data-dismiss="modal">Odustani</button>
                </div>
            </form>
        </div>

    </div>

</div>

</div>

</body>
