@extends('layouts.admindashboard')
@section('title')
    Komentari
@endsection
@include('layouts.welcomehead')
@section('comments')
@foreach($comments as $key => $comment)
    <div class="container-fluid">
        <div class="row">
            <div class="panel">
                <div class="col-md-10 col-md-offset-1" id="comments">
                    {{ $comment->komentari }}<br>
                    {{ $comment->created_at }}
                </div>
            </div>
            <form action="/admin/komentari/{{ $comment->id }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-danger">Izbrisi Komentar</button>
            </form>
        </div>
    </div>
    @endforeach
<style>
    body {
        background-color: #f9f9f9;
    }
    #comments {
        background-color: #818181;
        border: black solid 1px;
        border-radius: 4px;
        box-shadow: grey 3px 3px 3px;
        margin: 15px;
        color: #c7ddef;
        width: 80%;
    }
    h1 {
        text-align: center;
        font-family: "Book Antiqua";
        font-weight: 600;
    }
    .btn {
        box-shadow: #757575 2px 2px 2px;
    }
</style>
@endsection