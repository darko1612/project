

<div class="modal fade" id="meni" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="frmArtikal">&times;</button>
                <h4 class="modal-title">Novi Artikal</h4>
            </div>
            <div class="modal-body">
                <form action="/admin/menu/" method="post" enctype="multipart/form-data">
                    <div class="row">
                        {{ csrf_field() }}
                        <div class="col-lg-4 col-sm-4">
                            <div class="form-group">
                                <input type="text" name="naziv" id="naziv" placeholder="Naziv Artikla" class="form-control">
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4">
                            <div class="form-group">
                                <input type="file"  name="slika" id="slika" placeholder="Slika" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="form-group">
                                <select name="kategorija" id="kategorija" class="form-control" placeholder="Kategorija">
                                    <option value="">Izaberi Kategoriju</option>
                                    @foreach($category as $key=>$cat)
                                    <option value="{{$cat->kategorija}}">{{$cat->kategorija}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="form-group">
                                <input type="text" name="cena" id="cena" placeholder="Cena" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="form-group">
                                <textarea style="overflow:auto;" name="sastojci" id="sastojci" rows="5" cols= "50" maxlength="255" placeholder="Sastojci" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="save" class="btn btn-info">Sacuvaj</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>




