<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> Admin</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <!-- FONTAWESOME STYLES-->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">
    <div >
        <div class="adjust-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="{{asset('/img/logo.png')}}" />
                </a>
            </div>
            <span class="logout-spn" >
                <a href="#" style="color:#fff;"> <ul class="nav navbar-nav navbar">
                    <!-- Authentication Links -->
                        @if (Auth::guest())

                        @else
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('admin.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                </ul></a>

                </span>
        </div>
    </div>
<!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav " id="main-menu">

                <li class="active-link">
                    <a href="/admin"><i class="fa fa-edit "></i>Admin</a>
                </li>



                <li>
                    <a href="/admin/menu"><i class="fa fa-qrcode "></i>Kontrola Menija</a>
                </li>
                <li>
                    <a href="/"><i class="fa fa-bar-chart-o"></i>Pocetna Strana</a>
                </li>

                <li>
                    <a href="/admin/komentari"><i class="fa fa-edit "></i>Komentari </a>
                </li>
                <li>
                    <a href=""><i class="fa fa-table "></i>My Link Four</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-edit "></i>My Link Five </a>
                </li>
            </ul>
        </div>

    </nav>
<!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">

                    @yield('content')
                    @yield('menu')
                    @yield('comments')
                    @yield('admin-login')
                    @yield('categories')
                    @yield("adminedit")
                </div>
            </div>
            <!-- /. ROW  -->            <hr />

            <!-- /. ROW  -->
        </div>
        <!-- /. PAGE INNER  -->



    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<div class="footer">

</div>


<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{asset('js/query-1.10.2.js')}}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- CUSTOM SCRIPTS -->
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>

</body>
</html>
