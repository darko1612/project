@include('layouts.apphead')
<body>
    <div id="app">
        @include('layouts.appnav')
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
