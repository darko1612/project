@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Navigaciona stranica za goste</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        Korisnik {{ Auth::user()->name }} je ulogovan!
                    </div>
                    <a href="/">Nazad na pocetnu</a>
                </div>
            </div>
        </div>
    </div>
@endsection
