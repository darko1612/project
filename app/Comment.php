<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $fillable = [
        'id',
        'komentari'
    ];
    public $timestamps = true;
}
