<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Menu;
use App\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer('*', function($view) {
            $dorucak = Menu::orderBy('id')->get();
            $category = Category::get();
            $view->with('dorucak', $dorucak)
                 ->with('category', $category);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
