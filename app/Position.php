<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    public $fillable = [
        'id',
        'naziv',
        'sastojci',
        'slika',
        'kategorija',
        'cena',
    ];
    public $timestamps = false;
}
