<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Menu extends Model
{
   public $fillable = [
       'id',
       'pozicija',
       'naziv',
       'sastojci',
       'slika',
       'kategorija',
       'cena',
   ];
   public $timestamps = false;
 
}

