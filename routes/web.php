<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::get('welcome', function () {
    return view('welcome');
});
Route::get('auth/login', function() {
    return view('auth/login');
});

Auth::routes();
Route::get('home', 'HomeController@index')->name('home');

Route::post('admin/menu/', 'MenuController@create')->name('admin.menu');

Route::post('admin/kategorije/','CategoriesController@addCategories')->name('admin.kategorije');
Route::get('admin/kategorije/', 'CategoriesController@show');
Route::DELETE('admin/kategorije/{id}/', 'CategoriesController@destroy');
Route::post('admin/{id}/kategorije', 'CategoriesController@edit');

Route::get('/{category}/welcome', 'CategoriesController@index');

Route::get('admin/menu', ['uses'=>'MenuController@store'])->name('admin/menu')->middleware('auth:admin');

Route::DELETE('admin/{admin}', 'MenuController@destroy');
Route::PUT('admin/{admin}', 'MenuController@update');
Route::get('admin/{admin}/edit', 'MenuController@edit');



Route::get('admin/{id}/menu', ['uses'=> "PositionController@moveUp"]);
Route::post('admin/menu/{id}', 'PositionController@moveDown');


Route::get('users/logout', 'Auth\LoginController@userLogout')->name('user.logout');

Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');


Route::get('/admin', 'AdminController@index')->name('admin.dashboard');

Route::post('/welcome', 'CommentsController@create')->name('comments');
Route::get('/admin/komentari', 'CommentsController@store')->name('store.comments')->middleware('auth:admin');
Route::DELETE('/admin/komentari/{id}', 'CommentsController@destroy')->name('delete.comments');


Route::post('admin/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::post('admin/password/reset', 'Auth\AdminResetPasswordController@reset');
Route::get('admin/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::get('admin/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');


Route::get('{id}/admin/menu', 'MenuController@sort');
Route::post('{id}/admin/menu', 'MenuController@sort');

